define([], function () {

	return function () {
		var set = [];
			suits = [ "H", "D", "C", "S"],
			numbered = [2,3,4,5,6,7,8,9,10],
			heads = ["J", "Q", "K", "A"];

		suits.forEach(function (s) {
			numbered.forEach(function (c) {
				set.push({
					"name": c,
					"value": c,
					"class": s + c
				});
			});
			heads.forEach(function(h) {
				set.push({
					"name": h,
					"value": (h === "A") ? 11 : 10,
					"class": s + h
				});
			});
		});
		return set;
	}
});