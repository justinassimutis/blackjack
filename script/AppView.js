define([
	"jquery",
	"underscore",
	"backbone",
	"script/getSet",
	"script/Player"
], function ($, _, backbone, getSet, Player ) {

	var AppView = function (options) {
		this.playerNames = options.players;
		this.players = [];
		this.deck = [];
		this.currentPlayerIdx = 0;
	};

	$.extend(AppView.prototype, {

		initDeck: function () {
			this.deck = shuffle(getSet());
		},

		registerPlayers: function () {
			this.playerNames.forEach(function(name) {
				this.players.push(new Player({
					name: name
				}));
			}, this);
			
		},

		registerDealer: function () {
			this.players.push(new Player({
				name: "Dealer",
				type: "dealer"
			}));
		},

		deal: function () {
			var x = 1;
			while (x <= 2) {
				this.players.forEach(function (player) {
					if (player.type === "dealer" && player.hand.length === 1) { return; }
					player.getCard(this.deck.pop());
				}, this);
				this.render();
				x++;
			}
		},

		loop: function(result, prevPlayerIdx) {
			var promise = $.Deferred(),
				nextPlayer,
				prevPlayer,
				idx;

			// define next player
			idx = this.whoseTurn(result);
			nextPlayer = this.players[idx];

			prevPlayer = this.players[prevPlayerIdx];

			if (result === "hit") {
				prevPlayer.getCard(this.deck.pop());
				this.render();
			}

			if (result === "busted") {
				this.render();
			}

			if (result === "stick" || result === "busted" ) {
				this.playerButtons(prevPlayer, "disable");
			}

			if (nextPlayer.type === "dealer") {
				this.handleDealer(nextPlayer);
				return;
			}


			this.playerButtons(nextPlayer, "enable");
			nextPlayer.decision(promise);

			$.when(promise).then(function (result) {
				this.loop(result, idx);
			}.bind(this));			
		},

		handleDealer: function (dealer) {
			var active,
				player;

			console.log("handling dealer");

			active = this.players.filter(function(player) {
				return player.type !== "dealer" && !player.busted;
			});

			if (active.length < 1) {
				this.announce("LOSS");
				return;
			}

			player = active[0];
			
			while (dealer.total < 17) {
				dealer.getCard(this.deck.pop());
				this.render();
			}

			if (dealer.total > 21) { this.announce("WIN");}

			if (player.total > dealer.total) {
				this.announce("WIN");
			}

			if (player.total === dealer.total) {
				this.announce("DRAW");
			}
		},

		announce: function (result) {
			var template = '<div class="result alert {alert-status}">{message}</div>',
				resObj = {
					"WIN": {
						"bootstrap": "alert-success",
						"message": "PLAYER WINS"
					},
					"DRAW": {
						"bootstrap": "alert-info",
						"message": "DRAW"
					},
					"LOSS": {
						"bootstrap": "alert-danger",
						"message": "PLAYER LOSSES"
					}
				};

			var message = template.replace("{alert-status}", resObj[result].bootstrap)
					.replace("{message}", resObj[result].message);

			$("body").prepend(message);
		},

		whoseTurn: function (result) {
			if (result && result === "busted" || result === "stick") {
				this.currentPlayerIdx++;
			}

			return this.currentPlayerIdx;
		},

		setBindings: function () {
			this.players.forEach(function(player) {
				if (player.type === "player") {
					$("body").on("click", "." + player.type + " .btn.hit", function(event) {
						player.hit();
					});

					$("body").on("click", "." + player.type + " .btn.stick", function(event) {
						player.stick();
					});
				}
			}, this);
			$("body").on("click", ".new-game", this.reset.bind(this));
		},

		registerSlots: function () {
			this.players.forEach(function(player) {
				player.$container = $("." + player.type + ".panel");
				player.$cardSlot = $("." + player.type + " .cards");
				player.$result = $("." + player.type + " .score");
			});
		},

		playerButtons: function (player, action) {
			if (action === "enable") {
				player.$container.find(".btn.disabled").removeClass("disabled");
			} else if (action === "disable") {
				player.$container.find(".btn").addClass("disabled");
			}	
		},

		render: function() {
			var cardString = '<div class="card {card_class}"></div>';
			this.players.forEach(function(player) {
				player.hand.forEach(function(card, idx) {
					if (!card.rendered) {
						player.$cardSlot.append(
							cardString.replace("{card_class}", card.class)
						);
						card.rendered = true;
					}
				});
				player.$result.html(player.total);
			});
		},

		reset: function () {
			$("body").off("click");
			$(".result").remove();

			this.players.forEach(function(player) {
				player.$cardSlot.empty();
			});

			this.players = [];
			this.deck = [];
			this.currentPlayerIdx = 0;
			this.init();
		},

		init: function () {
			this.initDeck();
			this.registerPlayers();
			this.registerDealer();
			this.registerSlots();
			this.setBindings();
			this.deal();
			this.loop();
		}
	});

   return AppView;
});