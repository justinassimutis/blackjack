requirejs.config({
	baseUrl: ".",
	paths: {
		"jquery": "node_modules/jquery/dist/jquery",
		"underscore": "node_modules/underscore/underscore",
		"backbone": "node_modules/backbone/backbone",
		"AppView": "script/AppView"
	}
});

require(["jquery", "AppView"], function ($, AppView) {
	$(function () {
		var players = ["John"],
			appView = new AppView({
				players: players
			});

		appView.init();
	});
});