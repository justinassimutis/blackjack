define([
		"jquery",
		"underscore",
		"backbone",
		"script/getSet"
], function ($, _, backbone, getSet) {

	var Player = function (options) {
		this.name = options.name;
		this.type = options.type || "player";
		this.hand = [];

		this.total = 0;
		this.previousTotal = 0;
		this.busted = false;	
		this.adjustable = true;
		this.lastCard = null;
		this.promise = null;

		this.$cardSlot = null;
		this.$container = null;
		this.$result = null;
		this.$hit = null;
		this.$stick = null;

	};

	$.extend(Player.prototype, {
		hit: function() {
			this.promise.resolve("hit");
		},

		stick: function() {
			this.promise.resolve("stick");
		},

		getCard: function (card) {
			// clone card in case we want to modify its value if applicable
			this.lastCard = $.extend({}, card);
			this.hand.push(this.lastCard);
			this.calcTotal();
		},

		calcTotal: function () {
			this.previousTotal = this.total;
			var total = this.hand
				.map(function(item) {
					return item.value;
				})
				.reduce(function (a, b, idx, array) {
					return a + b;
				});

			if (total > 21 && this.adjustable) {
				//adjust values in case there are some axes
				this.adjustValues();
			}

			if (total === 21) {
				if (this.promise) {
					this.promise.resolve("stick");
				}
			}

			this.total = total;	
		},

		adjustValues: function () {
			//adjust values depending on situation
			if (this.lastCard.name === "A") {
				this.lastCard.value = 1;
			} else {
				this.adjustable = false;
				this.busted = true;
			}

			this.calcTotal();
		},

		decision: function (promise) {
			// busted value calculated straight after card is given 
			// and total calculated
			this.promise = (this.busted) ? promise.resolve("busted") : promise;
		}
	});

	return Player;
});